<?php

require_once "vendor/econea/nusoap/src/nusoap.php";
require "start.php";

use Controllers\Users;
use Controllers\Wallets;
use Controllers\Transactions;

$namespace = "Epayco";
$server = new soap_server();
$server->configureWSDL("epaycoSOAP",$namespace);
$server->wsdl->schemaTargetNamespace = $namespace;

$server->wsdl->addComplexType(
    'RegistrarUsuario',
    'complexType',
    'struct',
    'all',
    '',
    array(
        'nombres' => array('name' => 'nombres', 'type'=>'xsd:string'),
        'documento' => array('name' => 'documento', 'type'=>'xsd:string'),
        'celular' => array('name' => 'celular', 'type'=>'xsd:string'),
        'email' => array('name' => 'email', 'type'=>'xsd:string')
    )
);

$server->wsdl->addComplexType(
    'ResponseRegistrar',
    'complexType',
    'struct',
    'all',
    '',
    array(
        'message_error' => array('name'=>'message_error', 'type'=>'xsd:string'),
        'cod_error' => array('name' => 'cod_error', 'type' => 'xsd:int'),
        'success' => array('name' => 'success', 'type' => 'xsd:boolean')
    )
);

$server->register(
    'RegistrarUsuarioService',
	array('RegistrarUsuario' => 'tns:RegistrarUsuario'),
	array('RegistrarUsuario' => 'tns:ResponseRegistrar'),
	$namespace,
	false,
	'rpc',
	'encoded',
	'funcion que registra usuario'
);

$server->wsdl->addComplexType(
    'RecargarDinero',
    'complexType',
    'struct',
    'all',
    '',
    array(
        'documento' => array('name' => 'documento', 'type'=>'xsd:string'),
        'celular' => array('name' => 'celular', 'type'=>'xsd:string'),
        'monto' => array('name' => 'monto', 'type'=>'xsd:string')
    )
);

$server->wsdl->addComplexType(
    'ResponseRecargar',
    'complexType',
    'struct',
    'all',
    '',
    array(
        'message_error' => array('name'=>'message_error', 'type'=>'xsd:string'),
        'cod_error' => array('name' => 'cod_error', 'type' => 'xsd:int'),
        'success' => array('name' => 'success', 'type' => 'xsd:boolean')
    )
);

$server->register(
    'RecargarDineroService',
	array('RecargarDinero' => 'tns:RecargarDinero'),
	array('RecargarDinero' => 'tns:ResponseRecargar'),
	$namespace,
	false,
	'rpc',
	'encoded',
	'funcion que recarga la billetera'
);

$server->wsdl->addComplexType(
    'ConsultarSaldo',
    'complexType',
    'struct',
    'all',
    '',
    array(
        'documento' => array('name' => 'documento', 'type'=>'xsd:string'),
        'celular' => array('name' => 'celular', 'type'=>'xsd:string')
    )
);

$server->wsdl->addComplexType(
    'ResponseConsultar',
    'complexType',
    'struct',
    'all',
    '',
    array(
        'message_error' => array('name'=>'message_error', 'type'=>'xsd:string'),
        'cod_error' => array('name' => 'cod_error', 'type' => 'xsd:int'),
        'success' => array('name' => 'success', 'type' => 'xsd:boolean'),
        'data'    => array('name' => 'data', 'type' => 'xsd:float')
    )
);

$server->register(
    'ConsultarSaldoService',
	array('ConsultarSaldo' => 'tns:ConsultarSaldo'),
	array('ConsultarSaldo' => 'tns:ResponseConsultar'),
	$namespace,
	false,
	'rpc',
	'encoded',
	'funcion que consulta saldo'
);



$server->wsdl->addComplexType(
    'PagarCuenta',
    'complexType',
    'struct',
    'all',
    '',
    array(
        'cuenta' => array('name' => 'cuenta', 'type'=>'xsd:string'),
        'documento' => array('name' => 'documento', 'type'=>'xsd:string'),
        'celular' => array('name' => 'celular', 'type'=>'xsd:string'),
        'monto' => array('name' => 'monto', 'type'=>'xsd:float')
    )
);

$server->wsdl->addComplexType(
    'ResponsePagar',
    'complexType',
    'struct',
    'all',
    '',
    array(
        'message_error' => array('name'=>'message_error', 'type'=>'xsd:string'),
        'cod_error' => array('name' => 'cod_error', 'type' => 'xsd:int'),
        'success' => array('name' => 'success', 'type' => 'xsd:boolean'),
        'data'    => array('name' => 'data', 'type' => 'xsd:float')
    )
);

$server->register(
    'PagarCuentaService',
	array('PagarCuenta' => 'tns:PagarCuenta'),
	array('PagarCuenta' => 'tns:ResponsePagar'),
	$namespace,
	false,
	'rpc',
	'encoded',
	'funcion para pagar cuenta'
);


$server->wsdl->addComplexType(
    'ValidarTransaccion',
    'complexType',
    'struct',
    'all',
    '',
    array(
        'token' => array('name' => 'token', 'type'=>'xsd:string'),
        'session_id' => array('name' => 'session_id', 'type'=>'xsd:string')
    )
);

$server->wsdl->addComplexType(
    'ResponseValidar',
    'complexType',
    'struct',
    'all',
    '',
    array(
        'message_error' => array('name'=>'message_error', 'type'=>'xsd:string'),
        'cod_error' => array('name' => 'cod_error', 'type' => 'xsd:int'),
        'success' => array('name' => 'success', 'type' => 'xsd:boolean'),
        'data'    => array('name' => 'data', 'type' => 'xsd:float')
    )
);

$server->register(
    'ValidarTransaccionService',
	array('ValidarTransaccion' => 'tns:PagarCuenta'),
	array('ValidarTransaccion' => 'tns:ResponseValidar'),
	$namespace,
	false,
	'rpc',
	'encoded',
	'funcion para pagar cuenta'
);

function RegistrarUsuarioService($request)
{
   // var_dump($request); die();
    $response = array();
    $response = Users::create_user($request['nombres'], $request['documento'], $request['celular'], $request['email']);

    return $response;
}

function RecargarDineroService($request)
{

    $response = array();
    $response = Wallets::recargar_dinero($request['documento'], $request['celular'], $request['monto']);


    return $response;
}

function ConsultarSaldoService($request)
{

    $response = array();
    $response = Wallets::consultar_saldo($request['documento'], $request['celular']);


    return $response;
}

function PagarCuentaService($request)
{

    $response = array();
    $response = Transactions::create_transaction($request['cuenta'], $request['documento'], $request['celular'], $request['monto']);


    return $response;
}

function ValidarTransaccionService($request)
{

    $response = array();
    $response = Transactions::validate_transaction($request['session_id'], $request['token']);


    return $response;
}

$POST_DATA = file_get_contents("php://input");
$server->service($POST_DATA);
exit();

?>