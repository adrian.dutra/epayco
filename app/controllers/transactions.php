<?php 

namespace Controllers;
use Models\User;
use Models\Wallet;
use Models\Transaction;
use Models\Token;

class Transactions {
    
    public static function create_transaction($accountnumber, $documento, $celular, $monto)
    {
        try {
        
        $validate = 0;
        $response = array();
        $random_number = random_int(100000, 999999);

        if(empty($accountnumber))
        {
            $message = "La cuenta no puede ser vacia.";
            $code = 400;
            $validate = 1;
        }

        if(empty($documento))
        {
            $message = "El documento no puede ser vacio.";
            $code = 400;
            $validate = 1;
        }

        if(empty($celular))
        {
            $message = "El celular no puede ser vacio.";
            $code = 400;
            $validate = 1;
        }
        
        if(empty($monto))
        {
            $message = "El monto no puede ser vacio.";
            $code = 400;
            $validate = 1;
        }

        $existente =  User::where('documento', $documento)
                        ->where('celular', $celular)
                        ->first();
        
        if(empty($existente))
        {
            $message = "El usuario no existe.";
            $code = 400;
            $validate = 1;    
        }

        $wallet = Wallet::where('user_id', $existente->id)->first();

        if(empty($wallet))
        {
            $message = "La billetera no existe.";
            $code = 400;
            $validate = 1;
        }

        if ($monto > $wallet->balance)
        {
            $message = "La billetera no cuenta con los fondos suficientes.";
            $code = 400;
            $validate = 1;
        }
        
        if ($validate)
        {
            $response['message_error'] = $message;
            $response['cod_error']     = $code; 
            $response['success']       = false;
            return $response;
        }

        $transaction = Transaction::create(['accountnumber' =>  $accountnumber, 'amount' => $monto, 'confirmed' => false, 'wallet_id' => $wallet->id]);


        if (!empty($transaction))
        {
            Token::create(['token' => $random_number, 'transaction_id' => $transaction]);
        }
        

        $response['message_error'] = "Finalizado correctamente.";
        $response['success']       = true;
        $response['cod_error']     = 200;
        $response['data']          = $transaction; 

        return $response;

        }
        catch(\Exception $e){ 

            $response['message_error'] = $e->getMessage();
            $response['success']       = false;
            $response['cod_error']     = 400;
            return $response;

        }
    }

    public static function validate_transaction($session_id, $token)
    {
        try {


            $transaction = Transaction::where('token', $token)
                                ->where('session_id', $session_id)
                                ->update([
                                        'confirmed'=> true, 
                                    ]);

            if (!empty($transaction))
            {
                $wallet =  Wallet::where('id', $transaction->wallet_id)
                                ->decrements('balance', $transaction->amount);
            }

            $response['message_error'] = "Finalizado correctamente.";
            $response['success']       = true;
            $response['cod_error']     = 200;
            $response['data']          = $wallet; 

            return $response;

        }catch(\Exception $e){ 

        $response['message_error'] = $e->getMessage();
        $response['success']       = false;
        $response['cod_error']     = 400;
        return $response;

    }
    }

}
?>