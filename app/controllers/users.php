<?php 

namespace Controllers;
use Models\User;
use Models\Wallet;

class Users {
    
    public static function create_user($nombre, $documento, $celular, $email)
    {
        try {
        $validate = 0;
        $response = array();

        if(empty($nombre))
        {
            $message = "El nombre no puede ser vacio.";
            $code = 400;
            $validate = 1;
        }

        if(empty($documento))
        {
            $message = "El documento no puede ser vacio.";
            $code = 400;
            $validate = 1;
        }

        if(empty($celular))
        {
            $message = "El celular no puede ser vacio.";
            $code = 400;
            $validate = 1;
        }

        if(empty($email))
        {
            $message = "El email no puede ser vacio.";
            $code = 400;
            $validate = 1;
        }

        $existente =  User::where('documento', $documento)
                            ->where('celular', $celular)
                            ->first();
                            
        if(!empty($existente))
        {
            $message = "No se pudo crear el usuario, ya existe.";
            $code = 400;
            $validate = 1;    
        }

        if ($validate)
        {
            $response['message_error'] = $message;
            $response['cod_error']     = $code; 
            $response['success']       = false;
            return $response;
        }

        $user = User::create(['nombre'=>$nombre, 'documento'=>$documento, 'email'=>$email, 'celular'=>$celular]);
        
        if ($user)
        {
            Wallet::create(['balance' => 0, 'user_id' => $user->id]);
        }

        $response['message_error'] = "Finalizado correctamente.";
        $response['success']       = true;
        $response['cod_error']     = 200;
        $response['data']          = $user; 

        return $response;

        }
        catch(\Exception $e){ 

            $response['message_error'] = $e->getMessage();
            $response['success']       = false;
            $response['cod_error']     = 400;
            return $response;

        }
    }

    public static function update_user($user_id, $nombre, $email){
        $user         = User::find($user_id);
        $user->nombre = $nombre;
        $user->email  = $email;
        $updated = $user->save();
        return $updated;
    }
    
    public static function delete_user($user_id){
        $user = User::find($user_id);
        $deleted = $user->delete();
        
        $wallet = Wallet::where('user_id', $user_id)->delete();
        
        return $deleted; 
    }
}
?>