<?php

namespace Controllers;
use Models\Wallet;
use Models\User;

class Wallets {

    public static function recargar_dinero($documento, $celular, $monto){

        try {
                $validate = 0;
             
                if(empty($documento))
                {
                    $message  = "El documento no puede ser vacio.";
                    $validate = 1;
                }
            
                if(empty($celular))
                {
                    $message  = "El celular no puede ser vacio.";
                    $validate = 1;
                }

                $user = User::where('documento', $documento)
                         ->where('celular', $celular)
                         ->first();
              
                if(empty($user))
                {
                    $message  = "El usuario no existe";
                    $validate = 1;
                }

                if($validate)
                {
                    $response['message_error'] = $message;
                    $response['success']       = false;
                    $response['cod_error']     = 400;
                }

                

                $wallet = Wallet::where('user_id', $user->id)
                                ->increment('balance', $monto);

                $response['message_error'] = "Finalizado correctamente.";
                $response['success']       = true;
                $response['cod_error']     = 200;
                $response['data']          = $wallet; 

                return $response;

        }catch(\Exception $e){ 

            $response['message_error'] = $e->getMessage();
            $response['success']       = false;
            $response['cod_error']     = 400;
            return $response;

        }
        
    }

    public static function consultar_saldo($documento, $celular)
    {
        try {
            
            $validate = 0;
            $user = User::where('documento', $documento)
                    ->where('celular', $celular)
                    ->first();
 
            if(empty($user))
            {
                $message  = "El usuario no existe";
                $validate = 1;
            }
            
            if($validate)
            {
                $response['message_error'] = $message;
                $response['success']       = false;
                $response['cod_error']     = 400;
            }

            $saldo = Wallet::where('user_id', $user->id)->first();
             
            $response['message_error'] = "Finalizado correctamente.";
            $response['success']       = true;
            $response['cod_error']     = 200;
            $response['data']          = $saldo->balance; 

            return $response;
            
        }catch(\Exception $e){ 

            $response['message_error'] = $e->getMessage();
            $response['success']       = false;
            $response['cod_error']     = 400;
            return $response;

        }
    
    }
    
    public static function delete_wallet($documento, $celular){

        $user = User::where('documento', $documento)
                    ->where('celular', $celular)
                    ->first();
        
        $deleted = Wallet::where('user_id', $user->id)->delete();
        
        return $deleted; 
    }

}