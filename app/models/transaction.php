<?php 

namespace Models;
use \Illuminate\Database\Eloquent\Model;

class Transaction extends Model {
    protected $table = 'transactions';
    protected $fillable = ['accountnumber', 'amount', 'type', 'confirmed', 'wallet_id'];
}